import cv2
import numpy as np
from . import dataproc_model as proc
from . import constants as const


class OMRExtract:
    def __init__(self, width=700, height=700, questions=10, choices=4,
                 answers=[0, 0, 0, 0, 0, 0, 0, 0, 0, 0], weight=[1, 1, 1, 1, 1, 1, 1, 1, 1, 1]):
        self.width = width
        self.height = height
        self.questions = questions
        self.choices = choices
        self.answers = answers
        self.weight = weight

    def get_exam_info(self, image):
        exam_id = proc.get_exam_id(image)
        self.questions = const.SUBJECT_IDS[exam_id]["questions"]
        self.choices = const.SUBJECT_IDS[exam_id]["choices"]
        self.answers = const.SUBJECT_IDS[exam_id]["answers"]
        self.weight = const.SUBJECT_IDS[exam_id]["weight"]

    def grade_exam(self, image, padding=5):
        self.get_exam_info(image)
        img, img_black_white = proc.preprocess(image, self.width, self.height)
        contours, _ = cv2.findContours(img_black_white, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        rect_contours = proc.rectangle_contour(contours)
        biggest_contour = proc.get_corner_points(rect_contours[0])

        if biggest_contour.size != 0:
            biggest_contour = proc.reorder(biggest_contour)
            x, y, w, h = cv2.boundingRect(biggest_contour)

            x += padding
            y += padding
            w -= 2 * padding
            h -= 3 * padding

            point1 = np.float32([
                [x, y],
                [x + w, y],
                [x, y + h],
                [x + w, y + h]
            ])
            point2 = np.float32([[0, 0], [self.width, 0], [0, self.height], [self.width, self.height]])
            matrix = cv2.getPerspectiveTransform(point1, point2)

            img_warped_colored = cv2.warpPerspective(img, matrix, (self.width, self.height))
            img_warped_gray = cv2.cvtColor(img_warped_colored, cv2.COLOR_BGR2GRAY)
            img_thresh = cv2.threshold(img_warped_gray, 50, 255, cv2.THRESH_BINARY_INV)[1]

            boxes = proc.split_boxes(img_thresh, self.questions, self.choices)
            my_pixel_val = np.zeros((self.questions, self.choices))
            count_col = 0
            count_row = 0

            for box in boxes:
                total_pixels = cv2.countNonZero(box)
                my_pixel_val[count_row][count_col] = total_pixels
                count_col += 1
                if count_col == self.choices:
                    count_row += 1
                    count_col = 0
            my_index = []

            for x in range(0, self.questions):
                arr = my_pixel_val[x]
                my_index_val = np.where(arr == np.amax(arr))
                my_index.append(my_index_val[0][0])

            grading = [self.weight[x] if self.answers[x] == my_index[x] else 0 for x in range(0, self.questions)]
            score = sum(grading)

            for q in range(self.questions):
                for c in range(self.choices):
                    x = (c * (self.width // self.choices))
                    y = (q * (self.height // self.questions))
                    w = self.width // self.choices
                    h = self.height // self.questions

                    if c == my_index[q]:
                        color = (0, 255, 0) if grading[q] != 0 else (0, 0, 255)
                        cv2.rectangle(img_warped_colored, (x, y), (x + w, y + h), color, 2)
                        cv2.putText(img_warped_colored, str(grading[q]), (x, y + h),
                                    cv2.FONT_HERSHEY_SIMPLEX, 1,  (0, 0, 0), 5)

            return img_warped_colored, score
