import cv2
import numpy as np
import keras
import pandas as pd
import pytesseract
from PIL import Image
from keras.models import Sequential
from keras.layers import Dense, Conv2D, MaxPool2D, Flatten, Dropout
from keras.callbacks import ReduceLROnPlateau
from keras.optimizers import SGD
from keras.preprocessing.image import ImageDataGenerator
from sklearn.model_selection import train_test_split


def rectangle_contour(contours):
    rectangle_contours = []
    for contour in contours:
        area = cv2.contourArea(contour)
        if area > 50:
            peri = cv2.arcLength(contour, True)
            approx = cv2.approxPolyDP(contour, 0.02 * peri, True)
            if len(approx) == 4:
                rectangle_contours.append(contour)
    rectangle_contours = sorted(rectangle_contours, key=cv2.contourArea, reverse=True)
    return rectangle_contours


def get_corner_points(contour):
    peri = cv2.arcLength(contour, True)
    approx = cv2.approxPolyDP(contour, 0.02 * peri, True)
    return approx


def reorder(points):
    points = points.reshape((4, 2))
    points_new = np.zeros((4, 1, 2), np.int32)
    add = points.sum(1)
    points_new[0] = points[np.argmin(add)]
    points_new[3] = points[np.argmax(add)]
    diff = np.diff(points, axis=1)
    points_new[1] = points[np.argmin(diff)]
    points_new[2] = points[np.argmax(diff)]
    return points_new


def split_boxes(img, questions, choices):
    rows = np.vsplit(img, questions)
    boxes = []
    for row in rows:
        cols = np.hsplit(row, choices)
        for box in cols:
            boxes.append(box)
    return boxes


def preprocess(image, width, height):
    img = cv2.resize(image, (width, height))
    img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    img_blur = cv2.GaussianBlur(img_gray, (5, 5), 1)
    img_black_white = cv2.adaptiveThreshold(img_blur, 255, cv2.ADAPTIVE_THRESH_MEAN_C,
                                            cv2.THRESH_BINARY_INV, 11, 12)
    return img, img_black_white


def get_exam_id(opencv_image):
    color_converted = cv2.cvtColor(opencv_image, cv2.COLOR_BGR2RGB)
    pil_image = Image.fromarray(color_converted)
    extracted_text = pytesseract.image_to_string(pil_image)
    exam_id = (extracted_text.partition("ID: ")[2]).split(' ')[0]

    return exam_id


def build_model():
    train_data = pd.read_csv("Models/dataset/mnist/mnist_train.csv")
    test_data = pd.read_csv("Models/dataset/mnist/mnist_test.csv")

    y_train = train_data['label']
    x_train = train_data.drop(labels=["label"], axis=1)

    x_train = x_train / 255.0
    test_data = test_data / 255.0

    x_train = x_train.values.reshape(-1, 28, 28, 1)
    test_data = test_data.values.reshape(-1, 28, 28, 1)

    y_train = keras.utils.to_categorical(y_train, num_classes=10)

    x_train, x_val, y_train, y_val = train_test_split(x_train,
                                                      y_train,
                                                      test_size=0.25,
                                                      random_state=20,
                                                      shuffle=True)

    model = Sequential()

    model.add(Conv2D(filters=32, kernel_size=(5, 5), padding='Same', activation='relu', input_shape=(28, 28, 1)))
    model.add(Conv2D(filters=32, kernel_size=(5, 5), padding='Same', activation='relu'))
    model.add(MaxPool2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Conv2D(filters=64, kernel_size=(3, 3),  padding='Same', activation='relu'))
    model.add(Conv2D(filters=64, kernel_size=(3, 3), padding='Same', activation='relu'))
    model.add(MaxPool2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Flatten())

    model.add(Dense(256, activation="relu"))
    model.add(Dropout(0.5))
    model.add(Dense(10, activation="softmax"))

    model.compile(optimizer=SGD(learning_rate=0.01), loss="categorical_crossentropy", metrics=["accuracy"])

    learning_rate_reduction = ReduceLROnPlateau(monitor='val_accuracy',
                                                patience=3,
                                                verbose=1,
                                                factor=0.5,
                                                min_lr=0.00001)

    epochs = 50
    batch_size = 64

    datagen = ImageDataGenerator(
        rotation_range=10,
        zoom_range=0.1,
        width_shift_range=0.1,
        height_shift_range=0.1,
        horizontal_flip=False,
        vertical_flip=False
    )

    datagen.fit(x_train)

    history = model.fit(datagen.flow(x_train,
                                     y_train,
                                     batch_size=batch_size),
                        epochs=epochs,
                        validation_data=(x_val, y_val),
                        verbose=1,
                        steps_per_epoch=x_train.shape[0] // batch_size,
                        callbacks=[learning_rate_reduction])

    model.save('Models/CNN_model.keras')
