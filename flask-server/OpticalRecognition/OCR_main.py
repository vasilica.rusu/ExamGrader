import cv2
import numpy as np
import keras
import os
from . import dataproc_model as proc


class OCRExtract:
    def __init__(self, width=700, height=700):
        self.width = width
        self.height = height

    @staticmethod
    def extract_roi(image, contour):
        x, y, w, h = cv2.boundingRect(contour)

        padding = 5
        x += padding
        y += padding
        w -= 2 * padding
        h -= 3 * padding

        x = max(x, 0)
        y = max(y, 0)
        w = max(w, 1)
        h = max(h, 1)
        x = min(x, image.shape[1] - 1)
        y = min(y, image.shape[0] - 1)
        w = min(w, image.shape[1] - x)
        h = min(h, image.shape[0] - y)

        roi = image[y:y + h, x:x + w]

        resized_roi = cv2.resize(roi, (28, 28))

        return resized_roi

    def extract_contour(self, image):
        height, width, _ = image.shape
        top_20_height = int(0.2 * height)

        top_20_image = image[:top_20_height, :]
        _, img_black_white = proc.preprocess(top_20_image, self.width, top_20_height)

        contours, _ = cv2.findContours(img_black_white, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        rect_contours = proc.rectangle_contour(contours)[0:6]

        rect_contours.sort(key=lambda contour: cv2.boundingRect(contour)[0])

        digit_contours = []
        for i in range(0, 6):
            digit_contours.append(OCRExtract.extract_roi(img_black_white, rect_contours[i]))

        return digit_contours

    def extract_student_mark(self, image):
        student_mark = 0
        script_directory = os.path.dirname(__file__)
        model_path = os.path.join(script_directory, 'Models', 'CNN_model.keras')
        model = keras.models.load_model(model_path)
        digit_contours = OCRExtract.extract_contour(self, image)

        for i, contour in enumerate(digit_contours):
            prediction = model.predict(np.array([contour]))
            predicted_digit = np.argmax(prediction)
            student_mark = student_mark*10 + predicted_digit

        return student_mark.item()
