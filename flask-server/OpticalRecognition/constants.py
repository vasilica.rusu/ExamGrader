SUBJECT_IDS = {
    "1": {
        "questions": 10,
        "choices": 4,
        "answers": [1, 1, 2, 3, 3, 2, 2, 0, 1, 3],
        "weight": [0.5, 0.5, 0.5, 0.5, 1, 1, 1, 1, 2, 2]
    },
    "2": {
        "questions": 10,
        "choices": 4,
        "answers": [0, 0, 1, 3, 3, 2, 2, 0, 1, 3],
        "weight": [0.5, 0.5, 0.5, 0.5, 1, 1, 1, 1, 2, 2]
    }
}
