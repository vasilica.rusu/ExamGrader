from OpticalRecognition.OCR_main import OCRExtract
import pytest
import cv2
import sys
sys.path.append("..")


@pytest.fixture
def ocr_extract():
    return OCRExtract()


def test_extract_student_mark_image1(ocr_extract):
    image = cv2.imread("Test_Inputs/test_image1.jpg")

    assert image is not None, "Error: Could not load image."

    student_mark = ocr_extract.extract_student_mark(image)
    print(f"Student mark: {student_mark}")

    assert student_mark == 689432
