from OpticalRecognition.OMR_main import OMRExtract
import pytest
import cv2
import sys
sys.path.append("..")


@pytest.fixture
def omr_extract():
    return OMRExtract()


def test_extract_student_grade_answer_set1(omr_extract):
    image = cv2.imread("Test_Inputs/test_image1.jpg")

    assert image is not None, "Error: Could not load image."

    _, student_grade = omr_extract.grade_exam(image)
    print(f"Student grade: {student_grade}")

    assert student_grade == 8.5
