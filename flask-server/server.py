import cv2
import numpy as np
from flask import Flask, request, jsonify
import base64
import sys
import atexit
import pypyodbc as odbc
import traceback
from OpticalRecognition.OMR_main import OMRExtract
from OpticalRecognition.OCR_main import OCRExtract

app = Flask(__name__)


class NoCorrespondingNameError(Exception):
    """Exception raised when no corresponding name is found for the given mark."""
    pass


class Database:

    # SQL DATABASE CONNECTION
    DRIVER_NAME = 'SQL SERVER'
    SERVER_NAME = 'server-name'
    DATABASE_NAME = 'ExamInfo'

    # uid=<username>;
    # pwd=<password>;
    connection_string = f"""
        DRIVER={{{DRIVER_NAME}}};
        SERVER={SERVER_NAME};
        DATABASE={DATABASE_NAME};
        Trust_Connection=yes;
    """
    try:
        conn = odbc.connect(connection_string)
    except Exception as e:
        print(e)
        print("Connection to database not established")
        sys.exit()
    else:
        cursor = conn.cursor()

    @staticmethod
    def save_in_database(score, mark, image):
        try:
            Database.cursor.execute("SELECT Nume FROM dbo.Student WHERE Marca = ?", (mark,))
            row = Database.cursor.fetchone()

            if not row:
                raise NoCorrespondingNameError(f"No corresponding name found for the given mark: {mark}")

            name = row[0]

            Database.cursor.execute("INSERT INTO dbo.ExamenInfo (Nota, Nume, Marca, Imagine) VALUES ( ?, ?, ?, ?)",
                                    (score, name, mark, odbc.Binary(image)))
        except NoCorrespondingNameError as e:
            print(e)
            raise NoCorrespondingNameError
        except Exception as e:
            Database.cursor.rollback()
            print(str(e))
            print("Transaction rolled back")
            raise Exception
        else:
            print("Values inserted successfully")
            Database.cursor.commit()

    @staticmethod
    def retrieve_decode_image():
        try:
            image_data = request.json.get('image')
            if not image_data:
                raise ValueError("Image data not provided")
        except Exception as e:
            raise ValueError(f"Error retrieving image data: {str(e)}")

        # Decode base64 image data
        try:
            image_bytes = base64.b64decode(image_data.split(',')[1])
        except Exception as e:
            raise ValueError(f"Error decoding base64 image data: {str(e)}")

        return image_bytes

    @staticmethod
    def cleanup():
        Database.cursor.close()


@app.route('/')
def welcome():
    return 'Welcome to Exam Grader App!'


@app.route('/process_image', methods=['POST'])
def process_image():
    try:
        image_bytes = Database.retrieve_decode_image()

        try:
            numpy_array = np.frombuffer(image_bytes, np.uint8)
            image = cv2.imdecode(numpy_array, cv2.IMREAD_COLOR)
            if image is None:
                raise ValueError("Error decoding image to OpenCV format")
        except Exception as e:
            raise ValueError(f"Error converting image bytes to OpenCV image: {str(e)}")

        graded_image = cv2.resize(cv2.imread("utils/error_image.png"), (700, 700))
        score = 0

        try:
            exam_grader = OMRExtract()
            graded_image, score = exam_grader.grade_exam(image)
        except IndexError as e:
            print("Not enough rectangles to extract")
        except Exception as e:
            print(str(e))

        try:
            _, buffer = cv2.imencode('.jpg', graded_image)
            processed_image_data = base64.b64encode(buffer).decode('utf-8')
        except Exception as e:
            raise ValueError(f"Error encoding processed image: {str(e)}")

        student_mark = 0

        try:
            ocr_extract = OCRExtract()
            student_mark = ocr_extract.extract_student_mark(image)
        except Exception as e:
            print(str(e))

        return jsonify({'processed_image': 'data:image/jpeg;base64,' + processed_image_data,
                        'correct_answers': score, 'student_mark': int(student_mark)})

    except Exception as e:
        traceback.print_exc()
        return jsonify({'error': str(e)}), 500

    
@app.route('/save_in_database', methods=['POST'])
def save_in_database():
    try:
        correct_answers = request.json.get('correctAnswers')
        extracted_text = request.json.get('extractedText')
        image_bytes = Database.retrieve_decode_image()

        if correct_answers is None or extracted_text is None or image_bytes is None:
            raise ValueError("Missing data in the request")

        Database.save_in_database(correct_answers, extracted_text, image_bytes)

        return jsonify({"status": "success"}), 200

    except Exception as e:
        return jsonify({"status": "error", "message": str(e)}), 500


@app.route('/get_saved_entries', methods=['GET'])
def get_saved_entries():
    try:
        Database.cursor.execute("SELECT Nota, Nume, Marca, Imagine FROM dbo.ExamenInfo")
        rows = Database.cursor.fetchall()

        saved_entries = []
        for row in rows:
            entry = {
                'score': row[0],
                'name': row[1],
                'mark': row[2],
                'image': base64.b64encode(row[3]).decode('utf-8')
            }
            saved_entries.append(entry)

        return jsonify({'saved_entries': saved_entries}), 200

    except Exception as e:
        return jsonify({'error': str(e)}), 500


@app.route('/get_recent_entries', methods=['GET'])
def get_recent_entries():
    try:
        Database.cursor.execute("SELECT TOP 3 Nota, Nume, Marca, Imagine FROM dbo.ExamenInfo ORDER BY Id_Examen DESC")
        rows = Database.cursor.fetchall()

        recent_entries = []
        for row in rows:
            entry = {
                'score': row[0],
                'name': row[1],
                'mark': row[2],
                'image': base64.b64encode(row[3]).decode('utf-8')
            }
            recent_entries.append(entry)

        return jsonify({'recent_entries': recent_entries}), 200

    except Exception as e:
        return jsonify({'error': str(e)}), 500


if __name__ == '__main__':
    atexit.register(Database.cleanup)
    app.run(debug=True, host='0.0.0.0')
