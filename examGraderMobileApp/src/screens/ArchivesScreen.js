import React, { useEffect, useState } from 'react';
import { View, Text, Image, StyleSheet, TextInput, FlatList } from 'react-native';
import { API_URL } from '@env';

const ArchivesScreen = () => {
    const [savedEntries, setSavedEntries] = useState([]);
    const [filteredEntries, setFilteredEntries] = useState([]);
    const [searchQuery, setSearchQuery] = useState('');

    const fetchSavedEntries = async () => {
    try {
      const response = await fetch(`${API_URL}/get_saved_entries`);
      if (!response.ok) {
        throw new Error('Failed to fetch saved entries');
      }
      const data = await response.json();
      setSavedEntries(data.saved_entries);
      setFilteredEntries(data.saved_entries);
    } catch (error) {
      console.error('Error fetching saved entries:', error);
    }
    };

    useEffect(() => {
        fetchSavedEntries();
    }, []);

    const handleSearch = (query) => {
        setSearchQuery(query);
        const filtered = savedEntries.filter(entry => entry.name.toLowerCase().includes(query.toLowerCase()));
        setFilteredEntries(filtered);
    };

    const renderEntry = ({ item }) => (
        <View style={styles.entryContainer}>
            <Text>Nota: {item.score}</Text>
            <Text>Nume: {item.name}</Text>
            <Text>Marca: {item.mark}</Text>
            <Image source={{ uri: `data:image/jpeg;base64,${item.image}` }} style={styles.entryImage} />
        </View>
    );

    return (
    <View style={styles.container}>
      <TextInput
        style={styles.searchInput}
        placeholder="Search by name"
        onChangeText={handleSearch}
        value={searchQuery}
      />
      <FlatList
        data={filteredEntries}
        renderItem={renderEntry}
        keyExtractor={(item, index) => `${item.mark}_${index}`}
        contentContainerStyle={styles.flatListContainer}
      />
    </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#164a41',
        padding: 20,
    },
    searchInput: {
        height: 40,
        borderWidth: 1,
        borderColor: '#ccc',
        borderRadius: 8,
        paddingHorizontal: 10,
        marginBottom: 10,
    },
    entryContainer: {
        backgroundColor: 'transparent',
        padding: 10,
        marginBottom: 10,
    },
    entryImage: {
        width: 200,
        height: 300,
        alignSelf: 'center',
        marginVertical: 10,
    },
    flatListContainer: {
        paddingBottom: 20,
    },
});

export default ArchivesScreen;
