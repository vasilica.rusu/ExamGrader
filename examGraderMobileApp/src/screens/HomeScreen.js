import React, { useEffect, useState } from 'react';
import {Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, FlatList } from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { API_URL } from '@env';

const HomeScreen = ({ navigation }) => {
    const [recentEntries, setRecentEntries] = useState([]);

    const fetchSavedEntries = async () => {
    try {
      const response = await fetch(`${API_URL}/get_recent_entries`);
      if (!response.ok) {
        throw new Error('Failed to fetch saved entries');
      }
      const data = await response.json();
      setRecentEntries(data.recent_entries);
    } catch (error) {
      console.error('Error fetching saved entries:', error);
    }
    };

    useEffect(() => {
        fetchSavedEntries();
    }, []);

    const handleRefresh = () => {
    fetchSavedEntries();
  };

    const renderEntry = ({ item }) => (
      <View style={styles.entryContainer}>
        <Image source={{ uri: `data:image/jpeg;base64,${item.image}` }} style={styles.entryImage} />
        <View style={{ marginLeft: 10 }}>
          <Text>Nota: {item.score}</Text>
          <Text>Nume: {item.name}</Text>
          <Text>Marca: {item.mark}</Text>
        </View>
      </View>
    );

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity style={styles.archiveButton} onPress={() => navigation.navigate("Archives")}>
          <Entypo name="archive" size={45} color="black" />
          <Text style={styles.archiveText}>Archives</Text>
        </TouchableOpacity>
        <Text style={styles.title}>Home Screen</Text>
      </View>
      <View style={styles.content}>
        <View style={styles.refreshButtonContainer}>
          <Text style={styles.recentlyAddedText}>Recently added</Text>
          <TouchableOpacity onPress={handleRefresh}>
            <FontAwesome name="refresh" size={30} color="white" />
          </TouchableOpacity>
        </View>
        <FlatList
          data={recentEntries}
          renderItem={renderEntry}
          keyExtractor={(item, index) => `${item.mark}_${index}`}
          contentContainerStyle={styles.flatListContainer}
        />
      </View>
      <TouchableOpacity style={styles.button} onPress={() => navigation.navigate("Scanner")}>
        <Image 
          style={styles.floatingButton}
          source={require('../images/camera-icon.png')}
        />
      </TouchableOpacity>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#164a41',
  },
  content: {
    flex: 1,
    padding: 20,
    marginTop: 90,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 20,
  },
  archiveButton: {
    position: 'absolute',
    top: 100,
    left: 50,
  },
  archiveText: {
    marginTop: 5,
    fontSize: 12,
    color: 'white',
  },
  title: {
    fontSize: 28,
    fontWeight: 'bold',
    textAlign: 'center',
    flex: 1,
  },
  button: {
    position: 'absolute',
    bottom: 40,
    right: 40,
  },
  recentlyAddedText: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'white',
    marginBottom: 5,
    marginRight: 30,
  },
  entryContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'transparent',
    padding: 10,
    marginBottom: 10,
  },
  entryImage: {
    width: 100,
    height: 100,
    marginLeft: 10,
    resizeMode: 'contain',
  },
   flatListContainer: {
    paddingBottom: 20,
   },
   refreshButtonContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
  },
  floatingButton: {
    resizeMode: 'contain',
    width: 50,
    height: 50,
  }
});

export default HomeScreen;