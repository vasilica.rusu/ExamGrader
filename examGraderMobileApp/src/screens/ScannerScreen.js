import React, { useState, useEffect } from 'react'
import { StyleSheet, View, Image, TouchableOpacity, Text, TextInput } from 'react-native'
import DocumentScanner from 'react-native-document-scanner-plugin'
import RNFS from 'react-native-fs';
import Modal from 'react-native-modal';
import { API_URL } from '@env';

export default ( {navigation} ) => {
  const [scannedImage, setScannedImage] = useState(null);
  const [scannedImageBase64, setScannedImageBase64] = useState(null);
  const [processedImage, setProcessedImage] = useState(null);
  const [correctAnswers, setCorrectAnswers] = useState(null);
  const [extractedText, setExtractedText] = useState(null);
  const [editableCorrectAnswers, setEditableCorrectAnswers] = useState('');
  const [editableExtractedText, setEditableExtractedText] = useState('');
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [modalMessage, setModalMessage] = useState('');

  const scanDocument = async () => {
    const { scannedImages } = await DocumentScanner.scanDocument();

    if (scannedImages.length > 0) {
        setScannedImage(scannedImages[0]);
    }
  }

  useEffect(() => {
    scanDocument()
  }, []);

  const processImage = async () => {
    try {
      const base64Image = await convertImageToBase64(scannedImage);
      setScannedImageBase64(base64Image);

      const response = await fetch(`${API_URL}/process_image`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ image: base64Image }),
      });

      const data = await response.json();
  
      if (data.processed_image) {
        setProcessedImage(data.processed_image);
        setCorrectAnswers(data.correct_answers);
        setExtractedText(data.student_mark);
        setEditableCorrectAnswers(data.correct_answers.toString());
        setEditableExtractedText(data.student_mark.toString());
      }
    } catch (error) {
      console.error('Error fetching server data:', error);
    }
  };
  
  const convertImageToBase64 = async (imageUri) => {
    try {
      const imageFile = await RNFS.readFile(imageUri, 'base64');
      return `data:image/jpeg;base64,${imageFile}`;
    } catch (error) {
      console.error('Error converting image to base64:', error);
      throw error;
    }
  };

  const handleSave =  async () => {
    const correctAnswersValue = parseFloat(editableCorrectAnswers);
    const extractedTextValue = parseInt(editableExtractedText, 10);

    setCorrectAnswers(correctAnswersValue);
    setExtractedText(extractedTextValue);
    
    try {
      const response = await fetch(`${API_URL}/save_in_database`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
              correctAnswers: correctAnswersValue,
              extractedText: extractedTextValue,
              image: scannedImageBase64
        })
      });

      if (response.ok) {
        setModalMessage('Data saved successfully');
        setIsModalVisible(true);
        console.log(modalMessage)
        const jsonResponse = await response.json();
      } else {
        setModalMessage('Error saving data');
        console.log(response.message);
        setIsModalVisible(true);
      }
    } catch (error) {
        console.error('Network error:', error);
    }
  };

  const closeModal = () => {
    setIsModalVisible(false);
  };

  return (
    <View style={styles.container}>
      {processedImage ? (
        <View style={styles.container}>
        <Image
          resizeMode="contain"
          style={{ width: '100%', height: '100%' }}
          source={{ uri: processedImage }}
        />
        {(correctAnswers !== null || extractedText != null) && (
            <View style={styles.editContainer}>
              <View style={styles.inputRow}>
                <Text style={styles.label}>Correct Answers: </Text>
                <TextInput
                  style={styles.input}
                  value={editableCorrectAnswers}
                  onChangeText={setEditableCorrectAnswers}
                  keyboardType="numeric"
                />
              </View>
              <View style={styles.inputRow}>
                <Text style={styles.label}>Student ID: </Text>
                <TextInput
                  style={styles.input}
                  value={editableExtractedText}
                  onChangeText={setEditableExtractedText}
                  keyboardType="numeric"
                />
              </View>
            </View>
        )}
        <View style={styles.alignButtons}>
          <TouchableOpacity
            style={[styles.saveBackButton]}
            onPress={() => navigation.navigate("Home")}
          >
            <Text style={{ color: 'white' }}>Back</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.saveBackButton]}
            onPress={handleSave}
          >
            <Text style={{ color: 'white' }}>Save</Text>
          </TouchableOpacity>
        </View>
        <Modal isVisible={isModalVisible} onBackdropPress={closeModal}>
            <View style={styles.modalContent}>
              <Text style={styles.modalText}>{modalMessage}</Text>
              <TouchableOpacity style={styles.modalButton} onPress={closeModal}>
                <Text style={styles.modalButtonText}>OK</Text>
              </TouchableOpacity>
            </View>
        </Modal>
    </View>
      ) : (
        scannedImage ? (
          <View style={styles.container}>
            <Image
              resizeMode="contain"
              style={{ width: '100%', height: '100%' }}
              source={{ uri: scannedImage }}
            />
            <TouchableOpacity
              style={styles.scanButton}
              onPress={processImage}
            >
              <Text style={{ color: 'white' }}>Grade Exam</Text>
            </TouchableOpacity>
          </View>
        ) : (
          <Image
            resizeMode="contain"
            style={{ width: '100%', height: '100%' }}
            source={require('../images/predefined-image.jpg')}
          />
        )
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    width: '100%',
    height: '100%'
  },
  editContainer: {
    position: 'absolute',
    top: 10,
    width: '100%',
    alignItems: 'center',
  },
  inputRow: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 10,
  },
  label: {
    fontWeight: '700',
    fontSize: 18,
    color: '#164a41',
    marginVertical: 10,
    marginLeft: 10,
  },
  input: {
    flex: 1,
    backgroundColor: 'white',
    color: 'black',
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 5,
    padding: 10,
    fontSize: 18,
  },
  scanButton: {
    position: 'absolute',
    bottom: 40,
    alignSelf: 'center',
    backgroundColor: '#164a41',
    padding: 10,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: 'white',
  },
  saveBackButton: {
    padding: 10,
    backgroundColor: '#164a41',
    borderColor: 'white',
    borderRadius: 5,
    flex: 1,
    marginHorizontal: 5,
    alignItems: 'center'
  },
  alignButtons: {
    position: 'absolute',
    bottom: 40,
    left: 10,
    right: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  modalContent: {
    backgroundColor: 'white',
    padding: 20,
    borderRadius: 10,
    alignItems: 'center',
  },
  modalText: {
    fontSize: 18,
    marginBottom: 10,
    color: 'black',
  },
  modalButton: {
    marginTop: 10,
    paddingVertical: 10,
    paddingHorizontal: 20,
    backgroundColor: '#3498db',
    borderRadius: 5,
  },
  modalButtonText: {
    color: 'white',
    fontSize: 16,
  },
})