import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Entypo from "react-native-vector-icons/Entypo";
import HomeScreen from "./src/screens/HomeScreen";
import ArchivesScreen from "./src/screens/ArchivesScreen";
import ScannerScreen from "./src/screens/ScannerScreen";

const Tab = createBottomTabNavigator()
const Stack = createStackNavigator()

function MyTabs() {
  return (
    <Tab.Navigator
    screenOptions={({ route }) => ({
      headerShown: false,
      tabBarActiveTintColor: 'white',
      tabBarStyle: {
        backgroundColor: '#104037',
     },
   })}
    >
      <Tab.Screen name="Home" component={HomeScreen}
        options={{ headerShown:false,
          tabBarIcon: ({focused}) => {
          return (
            <View>
              <Entypo name="home" size={34} color="black" />
            </View>
          )
          }
        }}
      />
      <Tab.Screen name="Archives" component={ArchivesScreen}
        options={{ headerShown:false,
          tabBarIcon: ({focused}) => {
          return (
            <View style={{alignItems: "center", justifyContent: "center"}}>
              <Entypo name="archive" size={34} color="black" />
            </View>
          )
          }
        }}
      />
    </Tab.Navigator>
  );
}

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
          <Stack.Screen name="MyTabs" component={MyTabs} options={{ headerShown:false }} />
          <Stack.Screen name="Scanner" component={ScannerScreen} options={{ headerShown:false }} />
        </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;