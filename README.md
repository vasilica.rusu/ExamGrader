# Exam Grader: corectarea automată a examenelor grilă



## Introducere

**Exam Grader** este un proiect care își propune corectarea automată a examenelor grilă, prin utilizarea de tehnologii, precum Optical Character Recognition și Optical Mark Recognition, făcând posibilă procesarea și corectarea examenelor, într-un mod eficient.


## GitLab Repository
Codul sursă al lucrării se află la adresa: https://gitlab.upt.ro/vasilica.rusu/ExamGrader

La acest repository au acces toți utilizatorii autentificați pe serverul gitlab.upt.ro.

## Sisteme suportate
Aplicația dezvoltată poate fi folosită pe sisteme Windows, macOS si Linux.

## Condiții prealabile
- Versiune [Python 3.11](https://www.python.org/downloads/) sau ulterioară.
- Bibliotecile python necesare, folosind comanda:
```cmd
pip install -r requirements.txt
```
- Versiune [Node.js v20.11.1](https://nodejs.org/en/download/package-manager) sau ulterioară.
- React Native Client:
```cmd
npm install -g react-native-cli
```
- Dependințele JavaScript necesare, folosind comenzile:
```cmd
cd examGraderMobileApp
npm install
```
- [SQL Server Management Studio](https://learn.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver16) sau o altă bază de date de tip SQL, având următoarele tabele:
```cmd
CREATE TABLE [dbo].[ExamenInfo] (
    [Id_Examen] INT PRIMARY KEY,
    [Nota] FLOAT NOT NULL,
    [Nume] VARCHAR(50) NOT NULL,
    [Marca] INT NOT NULL,
    [Imagine] VARBINARY(MAX) NOT NULL,
    FOREIGN KEY ([Marca]) REFERENCES [dbo].[Student]([Marca])
)
```
```cmd
CREATE TABLE [dbo].[Student] (
    [Marca] INT PRIMARY KEY,
    [Nume] VARCHAR(50) NOT NULL
)
```

## Rularea programului

1. Se înlocuiește valoarea API_URL din fișierul examGraderMobileApp/.env, cu adresa și portul corespunzător configurației propriului dispozitiv. De exemplu: **http://192.168.78.102:5000**
2. Se pornește serverul flask, folosind comenzile:
```cmd
cd flask-server
python server.py flask run --host=0.0.0.0
```
3. Se pornește aplicația mobilă, folosind comenzile:
```cmd
cd examGraderMobileApp
npx react-native start
```
4. Tastează „a” în terminal, dacă folosești un dispozitiv Android, sau „i”, pentru un dispozitiv iOS.

## Rularea pe telefonul mobil

Informații adiționale legate de rularea pe dispozitivul mobil, se găsesc aici: https://reactnative.dev/docs/running-on-device